const request = require('request')

const forecast = (latitude,longitude,callback)=> {
    const url = "http://api.weatherstack.com/current?access_key=5227672293bf49f85c1a23d358c3b2f5&query="+latitude+','+longitude+"&units=m"

    request({ url, json: true }, (error, {body}) => {
        if (error) {
            callback('Unable to connect to weather service',undefined)
        } else if (body.error) {
            callback('Unable to find location',undefined)
        } else {
            callback(undefined, body.current.weather_descriptions[0]+ '. It is currently '+ body.current.temperature +' degrees out. It feels like '+ 
            body.current.feelslike+ ' degrees out. Local time is '+ body.location.localtime)
        }
    })    
}

module.exports = forecast